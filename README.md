# Licensecc

*适用于Windows和Linux的复制保护，许可库和许可证生成器。*

[![Standard](https://img.shields.io/badge/c%2B%2B-11-blue.svg)](https://en.wikipedia.org/wiki/C%2B%2B#Standardization)
[![unstable](http://badges.github.io/stability-badges/dist/unstable.svg)](http://github.com/badges/stability-badges)
[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)
[![travis](https://travis-ci.org/open-license-manager/licensecc.svg?branch=develop)](https://travis-ci.org/open-license-manager/licensecc)
[![Github_CI](https://github.com/open-license-manager/licensecc/workflows/Github_CI/badge.svg)](https://github.com/open-license-manager/licensecc/actions)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/81a1f6bc15014618934fc5fab4d3c206)](https://www.codacy.com/gh/open-license-manager/licensecc/dashboard?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=open-license-manager/licensecc&amp;utm_campaign=Badge_Grade)
[![codecov](https://codecov.io/gh/open-license-manager/licensecc/branch/develop/graph/badge.svg?token=vdrBBzX6Rl)](https://codecov.io/gh/open-license-manager/licensecc)
[![Github Issues](https://img.shields.io/github/issues/open-license-manager/licensecc)](http://github.com/open-license-manager/licensecc/issues)
保护您开发的软件免受未经授权的复制，将使用时间限制在一组特定的机器，或阻止在虚拟化环境中使用。它是一个开源许可证管理器，有助于保持您的软件关闭。在其他功能中，如果它在 “真实硬件” 上运行，它可以生成该硬件的签名并报告签名是否不匹配。

一个全面的 [功能列表](http://open-license-manager.github.io/licensecc/analysis/features.html),并且它们的状态在项目站点中可用。

如果您遇到问题，或者您只需要在 [文档](http://open-license-manager.github.io/licensecc)  请联系我们 [github discussions](https://github.com/open-license-manager/licensecc/discussions),我们很乐意帮忙。 

记得要表达你的感激之情给我们一个 <a class="github-button" href="https://gitee.com/ia0l6/licensecc" data-icon="octicon-star" aria-label="Star open-license-manager/licensecc on GitHub">star</a> Gitee

## License
该项目捐赠给社区。每个人都有使用的自由，而且永远都是。它有一个许可模式，允许在商业软件中自由修改和使用。 [BSD 3 clauses](https://opensource.org/licenses/BSD-3-Clause)  

## 项目结构
该软件由4个主要子组件组成:
-   一个C++库与一个很好的C API, `licensecc` 具有最小的 (或没有) 外部依赖 (您必须在软件中集成的部分)，即您当前所在的项目。
-   许可证调试器 `lcc-inspector` 在存在许可问题时将其发送给最终客户，或用于在发放许可证之前计算pc哈希。
-   许可证生成器 (gitee project [lcc-license-generator](https://gitee.com/ia0l6/lcc-license-generator)) `lccgen` 用于自定义库并生成许可证。
-   示例 [examples](https://gitee.com/ia0l6/examples) 以简化项目中的集成。
 
## 怎样去构建
在基本构建过程的概述下面，您可以找到有关的详细说明 [Linux](http://open-license-manager.github.io/licensecc/development/Build-the-library.html) 
或者 [Windows](http://open-license-manager.github.io/licensecc/development/Build-the-library-windows.html) 在项目网站中。 

### 先决条件
-   操作系统 : Linux(Ubuntu, CentOS), Windows
-   编译器   : GCC (Linux) MINGW (适用于Windows的Linux交叉编译), MINGW or MSVC (Windows) 
-   工具     : cmake(>3.6), git, make/ninja(linux)
-   类库     : 如果系统是Linux，则需要Openssl。Windows仅依赖于系统库。Boost是构建许可证生成器和运行测试所必需的，但它不是`licensecc`最终的依赖类库 

有关依赖项和支持的环境的完整列表，请查看 [项目网站](http://open-license-manager.github.io/licensecc/development/Dependencies.html)

克隆项目。它有子模块，不要忘记 '-recursive' 选项。

```console
git clone --recursive https://gitee.com/ia0l6/licensecc.git
cd licensecc/build
```

### 在Linux上构建

```console
cmake .. -DCMAKE_INSTALL_PREFIX=../install
make
make install
```

### 在Windows上构建 (with MSVC 2017)

```console
cmake .. -G "Visual Studio 15 2017 Win64" -DBOOST_ROOT="{Folder where boost is}" -DCMAKE_INSTALL_PREFIX=../install
cmake --build . --target install --config Release
```

### 在Linux上使用MINGW进行交叉编译

```console
x86_64-w64-mingw32.static-cmake .. -DCMAKE_INSTALL_PREFIX=../install
make
make install
```

## 怎么去测试

### 在Linux

```console
make test
```

### 在Windows (MSVC)

```console
ctest -C Release
```

## 怎么使用

这个 [示例](https://gitee.com/ia0l6/examples) 仓库列举了如何将 `licensecc` 迁移进你的项目。

## 如何贡献

您可以解决问题或寻求帮助的最简单方法是通过 [discussions tab](https://github.com/open-license-manager/licensecc/discussions) 上面，否则如果您认为有问题，您可以在 [issue system](https://gitee.com/ia0l6/licensecc/issues). 
看看 [contribution guidelines](CONTRIBUTING.md) 报告前。
我们使用 [GitFlow](https://datasift.github.io/gitflow/IntroducingGitFlow.html) (or at least a subset of it). 
请记住安装gitflow git插件，并使用 “develop” 作为拉取请求的默认分支。
